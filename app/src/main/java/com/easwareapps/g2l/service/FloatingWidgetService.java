/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.service;



import java.io.File;

import com.easwareapps.g2l.GestureLauncher;
import com.easwareapps.g2l.utils.OnSwipeTouchListener;
import com.easwareapps.g2l.R;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;


public class FloatingWidgetService extends Service {

	private static final int VERTICAL = 0;
	private static final int HORIZONDAL = 1;

	@SuppressLint("RtlHardcoded")
	private static final int LEFT = 0;
	@SuppressLint("RtlHardcoded")
	private static final int RIGHT = 1;

	WindowManager.LayoutParams floatingWidgetParams = null;
	WindowManager.LayoutParams sideBarParams = null;
	WindowManager.LayoutParams closeIconParams = null;
	ImageView floatingIcon = null;
	ImageView swipeLaunch = null;
	ImageView closeIcon= null;
	WindowManager wmgr = null;

	int statusBarHeight;


	double sizes[] = new double[10];
	int swipeType = 1;
	int lastAction = -1;
	int currentAction;
	long lastActionTime = -1;
	int allowedTimeDiff = 150;

	int WIDTH;
	int HEIGHT;
	int SIZE = 320;


	
	IBinder mBinder = new LocalBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public class LocalBinder extends Binder {
		public FloatingWidgetService getServerInstance() {
			return FloatingWidgetService.this;
		}


	}

	public boolean stopFloatingWidget(){
		if(floatingIcon != null){
			try{
				wmgr.removeView(floatingIcon);			
				floatingIcon = null;
				if(swipeLaunch == null){
					stopSelf();
					return true;
				}
			}catch(Exception e){}

		}
		return false;
	}



	public void adjustQuickLauncherTypeAndPosition(){

		try{
			SharedPreferences g2lPref = this.getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
			try{							
				if(wmgr == null)
					wmgr = (WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
				Log.d("IF ANS", (g2lPref.getBoolean("enable_quick_launch", true) &&
						g2lPref.getBoolean("keyboard_not_active", true)) + "");
				if(g2lPref.getBoolean("enable_quick_launch", true)){
					Log.d("QL", "staring quick launch");
					startFloatingWidget();
				}else{
					stopFloatingWidget();
				}if(g2lPref.getBoolean("enable_swipe_launch", true)){
					if(!checkForCrashes()){
						Log.d("QL", "staring swipe launch");
						startSwipeLaunch();
					}

				}else{
					stopSwipeLaunch();
				}


			}catch (Exception e) {			
				e.printStackTrace();
			}
			if(floatingIcon == null && swipeLaunch == null){
				Log.d("QL", "stopping...");
				stopSelf();
			}

		}catch(Exception E){

		}

	}

	public void changeFloatingIcon(final boolean visibilty){
		
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
		     public void run() {
		    	 try{
					if(floatingIcon != null){
						if(visibilty)
							wmgr.addView(floatingIcon, floatingWidgetParams);
						else
							wmgr.removeView(floatingIcon);
							
						//floatingIcon.setVisibility(view);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
		     }
		});
	}


	private boolean checkForCrashes() {
		String appPath = Environment.getExternalStorageDirectory()+"/.g2l/";

		File appDir = new File(appPath);
		if(!(appDir.exists() && appDir.isDirectory())){
			appDir.mkdir();
		}

		File storeFile = new File(appPath+"/crash");
		if(storeFile.exists()){
			return true;
		}

		return false;
	}


	private void startSwipeLaunch(){
		removeSwipeLaunch();
		setHeightAndWidth();

		try {
			if(canDrawOverlay()) {
				setSwipeLaunchParams();
				wmgr.addView(swipeLaunch, sideBarParams);
			}
		} catch (Exception E) {

		}


	}


	private void removeSwipeLaunch() {
		if(swipeLaunch != null){
			try{			
				wmgr.removeView(swipeLaunch);
				sideBarParams = null;
				swipeLaunch = null;
			}catch(Exception E){

			}		
		}

	}

	public int dpToPx(int dp) {
		DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
		int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	private void setSwipeLaunchParams(){
		try{
			swipeLaunch = new ImageView(getApplicationContext());			
			sideBarParams =  new WindowManager.LayoutParams();			
			sideBarParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE ;
			sideBarParams.format = PixelFormat.RGBA_8888;
			swipeLaunch.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_sidebar_portrait));
			//swipeLaunch.setBackgroundColor(Color.rgb(0, (15+16*7), 255));
			sideBarParams.type = WindowManager.LayoutParams.TYPE_PHONE;			
			sideBarParams.width = dpToPx(10);
			sideBarParams.height = (HEIGHT - statusBarHeight)*2;

			int pos = getSwipeLaunchPosition();
			switch (pos) {
			case 0:
				sideBarParams.x = -WIDTH+10;
				sideBarParams.y = -HEIGHT + statusBarHeight;
				setSwipeLauchListner(VERTICAL);
				break;
			case 1:
				sideBarParams.x = +WIDTH-10;
				sideBarParams.y = -HEIGHT + statusBarHeight;
				setSwipeLauchListner(VERTICAL);
				break;
			case 2:
				sideBarParams.width = WIDTH * 2;
				sideBarParams.height = dpToPx(10);
				sideBarParams.x = -WIDTH;
				sideBarParams.y = +HEIGHT-statusBarHeight;
				setSwipeLauchListner(HORIZONDAL);
				break;
			case 3:
				sideBarParams.width = WIDTH * 2;
				sideBarParams.height = 10;
				sideBarParams.x = +WIDTH;
				sideBarParams.y = -HEIGHT + statusBarHeight;
				setSwipeLauchListner(HORIZONDAL);
				break;
			default:
				sideBarParams.x = -WIDTH;
				sideBarParams.y = -HEIGHT + statusBarHeight;
				setSwipeLauchListner(VERTICAL);
				break;
			}
		}catch (Exception e) {

		}
	}



	private int getSwipeLaunchPosition() {
		SharedPreferences g2lPref = this.getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
		int pos = g2lPref.getInt("swipe_launch_position", 0);
		return pos;
	}

	private int getSwipeType(){
		SharedPreferences g2lPref = this.getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
		int type = g2lPref.getInt("swipe_type", 1);
		return type;
	}




	private void setHeightAndWidth() {
		statusBarHeight = (int)Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
		try{
			WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();

			display.getSize(size);
			WIDTH = size.x/2;
			HEIGHT = size.y/2;
				


			
		}catch(Exception E){

		}
		
		Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        if(display.getRotation() == Surface.ROTATION_90 || display.getRotation() == Surface.ROTATION_270){
        	SIZE = WIDTH;
        }else{
        	SIZE = HEIGHT;
        }

	}

	

	private void setSwipeLauchListner(int mode){
		swipeType = getSwipeType();
		if(swipeType == 1){
			if(mode == VERTICAL){
				swipeLaunch.setOnTouchListener(new OnSwipeTouchListener() {
					public void onSwipeTop() {
						startGestureDrawingScreen();
					}public void onSwipeBottom() {
						startGestureDrawingScreen();
					}

				});
			}else if(mode == HORIZONDAL){
				swipeLaunch.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_sidebar_landscape));
				swipeLaunch.setOnTouchListener(new OnSwipeTouchListener() {
					public void onSwipeRight() {
						startGestureDrawingScreen();
					}public void onSwipeLeft() {
						startGestureDrawingScreen();
					}

				});
			}
		}else{
			if(mode == HORIZONDAL){
				swipeLaunch.setOnTouchListener(new OnSwipeTouchListener() {
					public void onSwipeTop() {
						startGestureDrawingScreen();
					}

				});
			}else if(mode == VERTICAL){
				int pos = getSwipeLaunchPosition();
				if(pos == LEFT){
					swipeLaunch.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_sidebar_landscape));
					swipeLaunch.setOnTouchListener(new OnSwipeTouchListener() {
						public void onSwipeRight() {
							startGestureDrawingScreen();
						}

					});
				}else if(pos == RIGHT){
					swipeLaunch.setOnTouchListener(new OnSwipeTouchListener() {
						public void onSwipeRight() {
							startGestureDrawingScreen();
						}public void onSwipeLeft() {
							startGestureDrawingScreen();
						}

					});
				}
			}
		}
	}




	private void startFloatingWidget(){

		if(floatingIcon != null){
			try{			
				wmgr.removeView(floatingIcon);				
				floatingIcon = null;				
				closeIcon = null;
			}catch(Exception E){

			}		
		}
		try{			
			closeIcon = new ImageView(getApplicationContext());
			closeIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_trash_closed));			
			closeIconParams = new WindowManager.LayoutParams();
			closeIconParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_SCALED;
			closeIconParams.format = PixelFormat.RGBA_8888;
			closeIconParams.type = WindowManager.LayoutParams.TYPE_PHONE;
			closeIconParams.x = 0;
			closeIconParams.y = -180;

			floatingWidgetParams = new WindowManager.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			floatingWidgetParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE ;
			floatingIcon = new ImageView(getApplicationContext());												

			floatingWidgetParams.format = PixelFormat.RGBA_8888;
			floatingWidgetParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;


			DisplayMetrics displayMetrics = new DisplayMetrics();
			((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

			int displayType = 0;

			switch (displayMetrics.densityDpi) {


			case DisplayMetrics.DENSITY_XHIGH:
				statusBarHeight = 38;
				closeIconParams.width = 175;
				closeIconParams.height = 175;
				displayType = 3;
				break;
			case DisplayMetrics.DENSITY_HIGH:
				statusBarHeight = 38;
				closeIconParams.width = 150;
				closeIconParams.height = 150;
				displayType = 2;
				break;
			case DisplayMetrics.DENSITY_MEDIUM:
				statusBarHeight = 25;
				closeIconParams.width = 100;
				closeIconParams.height = 100;
				displayType = 1;
				break;
			case DisplayMetrics.DENSITY_LOW:
				statusBarHeight = 19;
				closeIconParams.width = 75;
				closeIconParams.height = 75;
				displayType = 0;
				break;
			case DisplayMetrics.DENSITY_XXHIGH:
				statusBarHeight = 38;
				closeIconParams.width = 200;
				closeIconParams.height = 200;
				displayType = 4;
				break;

			default:
				closeIconParams.width = 100;
				closeIconParams.height = 100;
				statusBarHeight = 25;
				displayType = 1;
			}

			int pos = 6;
			int alpha = 255;
			int iconSizeIndex = displayType;
			SharedPreferences g2lPref = this.getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
			try{							
				alpha = g2lPref.getInt("quicklauncher_alpha", 255);				
				pos = g2lPref.getInt("quicklauncher_position", 0);

			}catch (Exception e) {
				pos = 9;
			}
			SharedPreferences.Editor editor = g2lPref.edit();
			try{
				iconSizeIndex = g2lPref.getInt("quicklauncher_size", -1);
				if(iconSizeIndex == -1){
					iconSizeIndex = displayType;
					editor.putInt("quicklauncher_size", displayType);
					editor.commit();
				}
			}catch(Exception E){
				iconSizeIndex = 2;
				editor.putInt("quicklauncher_size", displayType);
				editor.commit();
			}

			
			
			floatingWidgetParams.width = (int)(sizes[iconSizeIndex]);
			floatingWidgetParams.height = (int)(sizes[iconSizeIndex]);


			WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();	
			if (Build.VERSION.SDK_INT >= 13) {
				display.getSize(size);
				WIDTH = size.x/2;
				HEIGHT = size.y/2;
			}else{
				WIDTH = display.getWidth()/2;
				HEIGHT = display.getWidth()/2;
			}

			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
				int tmp = WIDTH;
				WIDTH = HEIGHT;
				HEIGHT = tmp;
			}

			switch(pos){
			case 1:floatingWidgetParams.x = -WIDTH;floatingWidgetParams.y = -HEIGHT + statusBarHeight;break;
			case 2:floatingWidgetParams.x = 0;floatingWidgetParams.y = -HEIGHT + statusBarHeight;break;
			case 3:floatingWidgetParams.x = WIDTH;floatingWidgetParams.y = -HEIGHT + statusBarHeight;break;
			case 4:floatingWidgetParams.x = -WIDTH;floatingWidgetParams.y = 0;break;
			case 5:floatingWidgetParams.x = 0;floatingWidgetParams.y = 0;break;
			case 6:floatingWidgetParams.x = WIDTH;floatingWidgetParams.y = 0;break;
			case 7:floatingWidgetParams.x = -WIDTH;floatingWidgetParams.y = HEIGHT;break;
			case 8:floatingWidgetParams.x = 0;floatingWidgetParams.y = HEIGHT;break;
			case 9:floatingWidgetParams.x = WIDTH;floatingWidgetParams.y = HEIGHT;break;
			default:
				try{
					floatingWidgetParams.x = g2lPref.getInt("quicklauncher_position_x", -WIDTH);
					floatingWidgetParams.y = g2lPref.getInt("quicklauncher_position_y", HEIGHT - statusBarHeight);
				}catch (Exception e) {
					floatingWidgetParams.x = -WIDTH;
					floatingWidgetParams.y = HEIGHT;
				}
			}

			/*
			if(floatingWidgetParams.y > HEIGHT-statusBarHeight) {
				floatingWidgetParams.y = HEIGHT - floatingWidgetParams.height;
			}
			if(floatingWidgetParams.y < -HEIGHT+statusBarHeight) {
				floatingWidgetParams.y = -HEIGHT +  floatingWidgetParams.height;
			}

			if(floatingWidgetParams.x > WIDTH) {
				floatingWidgetParams.x = WIDTH- floatingWidgetParams.width;
			}

			if(floatingWidgetParams.x < -WIDTH) {
				floatingWidgetParams.x = -WIDTH + floatingWidgetParams.width;
			}
			*/


			Bitmap bitmap = null;
			if(g2lPref.getBoolean("custom_quick_launch_icon", false)){
				File icon = new File(Environment.getExternalStorageDirectory(),
						".g2l/quick_launcher.png");
				if(icon.exists())
					bitmap =BitmapFactory.decodeFile(icon.getCanonicalPath());
			}
			if(bitmap == null) {
				bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_quick_launch);
			}
			floatingIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_ql_bg));
			floatingIcon.setImageBitmap(bitmap);
			floatingIcon.setAlpha(alpha);

			//floatingIcon.setOnClickListener(clickListener);
			//floatingIcon.setOnLongClickListener(longClickListener);
			floatingIcon.setOnTouchListener(touchListener);
			if(canDrawOverlay()) {
				wmgr.addView(floatingIcon, floatingWidgetParams);
			}
		}catch(Exception E){
			E.printStackTrace();
		}





	}


	View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			startGestureDrawingScreen();
		}
	};

	View.OnLongClickListener longClickListener = new View.OnLongClickListener() {

		@Override
		public boolean onLongClick(View view) {
			floatingIcon.setOnTouchListener(touchListener);
			floatingIcon.setOnClickListener(null);
			return false;
		}
	};

	private boolean canDrawOverlay() {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (!Settings.canDrawOverlays(this)) {
				Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
						Uri.parse("package:" + getPackageName()));
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(intent);
				return false;
			}
		}
		return true;
	}


	private void startGestureDrawingScreen() {

		try {
			Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(25);
		} catch (Exception vibEx){

		}


		try{
            Intent launchIntent = new Intent(this, GestureLauncher.class);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(launchIntent);

        }catch(Exception E){
            Toast.makeText(getApplicationContext(),
                    R.string.application_not_found, Toast.LENGTH_SHORT).show();
            System.out.println("Error"+E);
        }






	}			    


	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		try{
			setHeightAndWidth();
			adjustQuickLauncherTypeAndPosition();
		}catch(Exception e){}


	}



	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		sizes = new double[]{dpToPx(16), dpToPx(32), dpToPx(48), dpToPx(64), dpToPx(80),
							 dpToPx(96), dpToPx(102), dpToPx(118), dpToPx(134), dpToPx(150)
							};
		adjustQuickLauncherTypeAndPosition();
		return START_STICKY;	
	}


	public void saveQuickLauncherPosition() {
		SharedPreferences g2lPref = getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		g2lEditor.putInt("quicklauncher_position_x", floatingWidgetParams.x);
		g2lEditor.putInt("quicklauncher_position_y", floatingWidgetParams.y);
		g2lEditor.commit();

	}






	public void setQuickLaunchSize(int index) {
		
		if(floatingIcon != null){
			floatingWidgetParams.width = (int)(SIZE/sizes[index]);
			floatingWidgetParams.height = (int)(SIZE/sizes[index]);
			try{
				wmgr.updateViewLayout(floatingIcon, floatingWidgetParams);
			}catch(Exception e){}
		}

	}
	@SuppressWarnings("deprecation")
	public void setTransperancy(int alpha){
		if(alpha == -1){

			try{
				SharedPreferences g2lPref = this.getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
				alpha = g2lPref.getInt("quicklauncher_alpha", 255);

			}catch (Exception e) {
				// TODO: handle exception

			}

		}
		if(floatingIcon!=null){
			floatingIcon.setAlpha(alpha);
		}

	}



	@Override
	public void onLowMemory() {
				
		super.onLowMemory();
	}



	public boolean stopSwipeLaunch() {
		
		if(swipeLaunch != null){
			try{
				wmgr.removeView(swipeLaunch);
				swipeLaunch = null;
				if(floatingIcon == null){
					stopSelf();
					return true;
				}
			}catch(Exception E){
				E.printStackTrace();
			}
		}
		return false;
	}



	public void setQuickLaunchPosition() {
		
		if(floatingIcon!=null){
			adjustQuickLauncherTypeAndPosition();
		}

	}


    private float pressedX;
    private float pressedY;
    private static float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distance = (float) Math.sqrt(dx * dx + dy * dy);
        return distance;
    }
	View.OnTouchListener touchListener = new View.OnTouchListener() {
			float x, y;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (floatingIcon == null) {
					return true;
				}

				Log.e("ACTION", MotionEvent.ACTION_DOWN + " | "+ event.getAction());


				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
                        pressedX = event.getX();
                        pressedY = event.getY();
						lastActionTime = System.currentTimeMillis();
						lastAction = MotionEvent.ACTION_DOWN;
						x = event.getRawX() - floatingWidgetParams.x;
						y = event.getRawY() - floatingWidgetParams.y;
						floatingWidgetParams.x = Math.round(event.getRawX() - x);
						floatingWidgetParams.y = Math.round(event.getRawY() - y);
						wmgr.updateViewLayout(floatingIcon, floatingWidgetParams);

						return true;
					case MotionEvent.ACTION_UP:
						try {
							long timeDiff = System.currentTimeMillis() - lastActionTime;
                            float distanceDiff = distance(pressedX, pressedY, event.getX(), event.getY());
							if ( timeDiff <= allowedTimeDiff
                                     && distanceDiff == 0) {
								try {
									wmgr.removeView(closeIcon);
								} catch (Exception e) {

								}
								lastAction = MotionEvent.ACTION_UP;
								startGestureDrawingScreen();
								return true;

							}
							lastAction = MotionEvent.ACTION_UP;
						} catch (Exception f) {

						}
						lastAction = MotionEvent.ACTION_UP;
						lastAction = currentAction;
						int cx = closeIconParams.x;
						int cy = closeIconParams.y;
						int cs = closeIconParams.width;
						if (floatingWidgetParams.x > (cx - cs) &&
								floatingWidgetParams.x < (cx + cs) &&
								floatingWidgetParams.y > (cy - cs) &&
								floatingWidgetParams.y < (cy + cs)) {

							if (swipeLaunch == null) {
								stopSelf();
								//System.exit(0);
							}
							try {
								SharedPreferences g2lPref = getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
								SharedPreferences.Editor editor = g2lPref.edit();
								editor.putBoolean("enable_quick_launch", false);
								editor.commit();
								wmgr.removeView(floatingIcon);
								floatingIcon = null;
								wmgr.removeView(closeIcon);
								closeIcon = null;
							} catch (Exception e) {
								wmgr.removeView(closeIcon);
								wmgr.removeView(floatingIcon);
							}


						} else {
							try {
								wmgr.removeView(closeIcon);
								SharedPreferences g2lPref = getSharedPreferences("com.easwareapps.g2l", MODE_PRIVATE);
								SharedPreferences.Editor editor = g2lPref.edit();
								editor.putInt("quicklauncher_position_x", floatingWidgetParams.x);
								editor.putInt("quicklauncher_position_y", floatingWidgetParams.y);
								editor.commit();

							} catch (Exception E) {
								try {
									wmgr.removeView(closeIcon);
								} catch (Exception e2) {

								}

							}
						}

//						floatingIcon.setOnTouchListener(null);
//						floatingIcon.setOnClickListener(clickListener);
						return true;
					case MotionEvent.ACTION_MOVE:

						lastAction = MotionEvent.ACTION_MOVE;
						try {
							floatingWidgetParams.x = Math.round(event.getRawX() - x);
							floatingWidgetParams.y = Math.round(event.getRawY() - y);
							wmgr.updateViewLayout(floatingIcon, floatingWidgetParams);
						} catch (Exception e) {

						}


						cx = closeIconParams.x;
						cy = closeIconParams.y;
						cs = closeIconParams.width;

						if (floatingWidgetParams.x > (cx - cs) && floatingWidgetParams.x < (cx + cs) && floatingWidgetParams.y > (cy - cs) && floatingWidgetParams.y < (cy + cs)) {
							closeIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_trash_open));
						} else {
							closeIcon.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_trash_closed));
						}
						try {
							wmgr.updateViewLayout(closeIcon, closeIconParams);
						} catch (Exception E) {
							try {
								wmgr.addView(closeIcon, closeIconParams);
							} catch (Exception e) {

							}
						}
						return true;


				}
				return true;
			}
	};



	//TODO FUTURE
//	private void startGestureLauncher() {
//		WindowManager.LayoutParams gestureDrawringParams = new WindowManager.LayoutParams();
//		gestureDrawringParams.flags = WindowManager.LayoutParams.FLAG_LOCAL_FOCUS_MODE | WindowManager.LayoutParams.FLAG_SCALED;
//		LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		final View gestureScreen = inflater.inflate(R.layout.gesture_launcher, w);
//		gestureDrawringParams.type = WindowManager.LayoutParams.TYPE_PHONE;
//
//
//		gestureDrawringParams.format = PixelFormat.RGBA_4444;
//		gestureScreen.setBackgroundColor(Color.rgb(0, 0, 0));
//
//		RelativeLayout rl = (RelativeLayout) gestureScreen.findViewById(R.id.idGestureLauncherRelLayout);
//		rl.setBackgroundColor(Color.WHITE);
//
//
//
//		GestureOverlayView gov = (GestureOverlayView) gestureScreen.findViewById(R.id.gestureOverlayView);
//		gov.setBackgroundColor(Color.BLACK);
//		gov.addOnGesturePerformedListener(new GestureOverlayView.OnGesturePerformedListener() {
//			@Override
//			public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
//				Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();
//				wmgr.removeView(gestureScreen);
//			}
//		});
//
//		wmgr.addView(gestureScreen, gestureDrawringParams);
//
//
//	}



	



}