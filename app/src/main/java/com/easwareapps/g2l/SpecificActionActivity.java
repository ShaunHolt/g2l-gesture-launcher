/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.easwareapps.g2l.fragment.SelectAppFragment;
import com.easwareapps.g2l.fragment.SelectContactFragment;
import com.easwareapps.g2l.fragment.SelectSpecificActionFragment;
import com.easwareapps.g2l.fragment.SelectURLFragment;

public class SpecificActionActivity extends AppCompatActivity {

	SelectAppFragment appFragment = null;
	SelectContactFragment contactFragment = null;
	SelectURLFragment urlFragment = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
				R.style.G2LTheme_NoActionBar ;
		setTheme(theme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.specific_action_layout);
		int position = getIntent().getIntExtra("ACTION_ID", 0);
		if(position == SelectSpecificActionFragment.APPS){
			if(appFragment == null)
				appFragment = new SelectAppFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_container, appFragment).commit();

		}else if(position == SelectSpecificActionFragment.URL){
			if(urlFragment == null)
				urlFragment = new SelectURLFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_container, urlFragment).commit();
		}else if(position == SelectSpecificActionFragment.CALL_MSG_MAIL){
			if(contactFragment == null)
				contactFragment = new SelectContactFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_container, contactFragment).commit();

		}else{
			SelectSpecificActionFragment fragment = new SelectSpecificActionFragment();
			getSupportFragmentManager().beginTransaction()
			.add(R.id.specific_action_container, fragment).commit();
		}

		enableToolar(position);
		
	}
	@Override
	public void onBackPressed() {
		finish();
		super.onBackPressed();
	}



	private void enableToolar(int position){
		String[] title = getResources().getStringArray(R.array.selection_list);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if(toolbar != null) {
			toolbar.setTitle(title[position]);
			toolbar.setTitleTextColor(Color.WHITE);
			toolbar.setNavigationIcon(R.mipmap.ic_back);
			setSupportActionBar(toolbar);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					finish();
				}
			});
		}




	}

	public boolean onCreateOptionsMenu(Menu menu) {


		if(getIntent().getIntExtra("ACTION_ID", 0)
					== SelectSpecificActionFragment.APPS
				|| getIntent().getIntExtra("ACTION_ID", 0)
					==SelectSpecificActionFragment.CALL_MSG_MAIL) {
			MenuInflater menuInflater = getMenuInflater();
			menuInflater.inflate(R.menu.main, menu);
			 
		}
		return super.onCreateOptionsMenu(menu);

	}


	
	


}
