/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.easwareapps.g2l.R;
import com.easwareapps.g2l.fragment.BackupRestoreFragment;
import com.easwareapps.g2l.fragment.GestureSettingFragment;
import com.easwareapps.g2l.fragment.OtherSettingsFragment;
import com.easwareapps.g2l.fragment.QuickLauncherFragment;
import com.easwareapps.g2l.fragment.SwipeLauncherFragment;

public class DetailedSettings extends AppCompatActivity {

	QuickLauncherFragment qlf = null;
	GestureSettingFragment gsf = null;
	SwipeLauncherFragment slf = null;
	OtherSettingsFragment osf = null;
	BackupRestoreFragment brf = null;
	Toolbar toolbar;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
				R.style.G2LTheme_NoActionBar ;
		setTheme(theme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.secondary_settings_layout);
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		
		if (toolbar != null) {
			try {
				String settingsTitle[] = getResources().getStringArray(R.array.settings_title);
				toolbar.setTitle(settingsTitle[getIntent().getIntExtra("settings_index", 0)]);
			}catch (Exception e) {

			}
			setSupportActionBar(toolbar);
			toolbar.setNavigationIcon(R.mipmap.ic_back);
			toolbar.setNavigationOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}
		init(getIntent().getIntExtra("settings_index", 0));

	}

	private void init(int index){

		switch (index) {
		case 0:
			if(qlf == null)
				qlf = new QuickLauncherFragment();
			getSupportFragmentManager().beginTransaction().
				replace(R.id.settings_list, qlf).commit();

			break;
		case 1:
			if(slf == null)
				slf = new SwipeLauncherFragment();
			getSupportFragmentManager().beginTransaction().
				replace(R.id.settings_list, slf).commit();

			break;
		case 2:
			if(gsf == null)
				gsf = new GestureSettingFragment();
			getSupportFragmentManager().beginTransaction().
				replace(R.id.settings_list, gsf).commit();

			break;
		case 3:
			if(osf == null)
				osf = new OtherSettingsFragment();
			getSupportFragmentManager().beginTransaction().
				replace(R.id.settings_list, osf).commit();

			break;
		case 4:
			if(brf ==null)
				brf = new BackupRestoreFragment();
			getSupportFragmentManager().beginTransaction().
			replace(R.id.settings_list, brf).commit();
			break;

		default:
			break;
		}


	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Toast.makeText(getApplicationContext(),
                requestCode + " | " + (brf == null), Toast.LENGTH_LONG).show();
		if(requestCode == 999 ) {

			if(isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
				if(brf != null) {
					brf.backup();
				}
			}
		} else if(requestCode == 998 ) {
			if(isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
				if(brf != null) {
					brf.showFileChooser();
				}
			}
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}


	@TargetApi(23)
	private boolean isPermissionGranted(String permission){
		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return true;
		}else {
			int hasStoragePermission = checkSelfPermission(permission);
			if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
				return false;

			}
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == QuickLauncherFragment.REQUEST_ICON_CHANGE) {
			if(qlf != null) {
				qlf.updateQuickLauncher();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
