/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easwareapps.g2l.AddGestureActivity;
import com.easwareapps.g2l.MainActivity;
import com.easwareapps.g2l.R;
import com.easwareapps.g2l.utils.BitmapManager;
import com.easwareapps.g2l.utils.DBHelper;
import com.easwareapps.g2l.utils.DataTransfer;
import com.easwareapps.g2l.utils.GestureInfo;

import java.util.ArrayList;


public class GestureAdapter extends RecyclerView.Adapter<GestureAdapter.ViewHolder> {


    boolean selectionStarted = false;
    Context context;
    LruCache<String, Bitmap> cache;
    int iconSize;
    static GestureAdapter instance = null;
    static RecyclerView rv;
    ActionMode actionMode;
    MainActivity activity;
    ArrayList<GestureInfo> gestures;



    public static GestureAdapter getInstance(Context context, int iconSize, RecyclerView rv1,
                                             MainActivity activity, ArrayList<GestureInfo> infos) {

        rv = rv1;
        if(instance == null) {
            instance = new GestureAdapter();
        }

        instance.iconSize = iconSize;
        instance.context = context;
        instance.activity = activity;
        instance.gestures = infos;

        return instance;
    }

    public GestureAdapter(){

        final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
        int cacheSize = maxMemory/8;
        cache = new LruCache<String, Bitmap>(cacheSize){

            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() - value.getHeight();
            }

        };

        rv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                selectionStarted = true;
                return true;
            }
        });

    }







    public void closeActionMode() {
        if(actionMode != null) {
            actionMode.finish();
        }
    }

    public void requestUpdate() {
        notifyDataSetChanged();
    }

    public int getSelectionColor() {
        return Color.GRAY;
    }

    public int getNormalColor() {
        SharedPreferences pref = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        return pref.getBoolean("enable_dark_theme", false) ? Color.BLACK: Color.WHITE;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView icon = null;
        TextView name = null;
        View mainView;
        public ViewHolder(View view){
            super(view);
            mainView = view;
            icon = (ImageView)view.findViewById(R.id.gesture_image);
            name = (TextView)view.findViewById(R.id.gesture_title);
            mainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setSelected(true);

                }
            });
            mainView.setOnClickListener(this);
            mainView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(!selectionStarted) {
                        selectionStarted = true;
                        //Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
                        actionMode = activity.startSupportActionMode(mActionModeCallback);
                        gestures.get(getAdapterPosition())
                                .setSelected(!gestures.get(getAdapterPosition()).isSelected());
                        notifyDataSetChanged();
                    }
                    return false;
                }
            });
        }


        @Override
        public void onClick(View view) {

            if(selectionStarted){
                gestures.get(getAdapterPosition())
                        .setSelected(!gestures.get(getAdapterPosition()).isSelected());
                notifyDataSetChanged();
            } else {
                Intent editGesture = new Intent(context, AddGestureActivity.class);
                DataTransfer.gestureName = gestures.get(getAdapterPosition()).getId();
                editGesture.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(editGesture);
            }
        }
    }

    public ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            try {
                FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
                fab.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            MenuInflater inflater = activity.getMenuInflater();
            inflater.inflate(R.menu.delete, menu);
            final MenuItem delete = menu.findItem(R.id.menu_delete);
            delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    for(GestureInfo gesture:gestures){
                        if(gesture.isSelected()) {
                            activity.deleteGesture(gesture.getId());

                        }
                    }
                    activity.loadGestureList();
                    notifyDataSetChanged();
                    return false;
                }
            });

            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

            selectionStarted = false;
            removeAllSelection();
            //Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
            FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
            fab.setVisibility(View.VISIBLE);
            //toolbar.setVisibility(View.VISIBLE);

        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
    };



    private void removeAllSelection() {

        for(GestureInfo gesture: gestures) {
            gesture.setSelected(false);
        }
        notifyDataSetChanged();
    }

    private void selectAll() {

        for(GestureInfo gesture: gestures) {
            gesture.setSelected(true);
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gesture_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



        holder.name.setText(gestures.get(position).getTitle());
        //final String imageKey = String.valueOf(appsDetails.get(position).pname);
        holder.icon.setLayoutParams(new LinearLayout.LayoutParams(iconSize, iconSize));
        holder.icon.setScaleType(ImageView.ScaleType.CENTER_CROP);

        final String imageKey = String.valueOf(gestures.get(position).getTitle());
        final Bitmap bitmap = cache.get(imageKey);
        if (bitmap != null) {
            holder.icon.setImageBitmap(bitmap);
        } else {
            Resources res = context.getResources();
            BitmapManager bm = new BitmapManager(holder.icon, res, cache);
            bm.setType(BitmapManager.FROMSTORAGE);
            final BitmapManager.AsyncDrawable asyncDrawable =
                    new BitmapManager.AsyncDrawable(context.getResources(), null, bm);
            holder.icon.setImageDrawable(asyncDrawable);
            bm.execute(gestures.get(position).getId());
        }

        holder.mainView.setSelected(gestures.get(position).isSelected());
        if(gestures.get(position).isSelected()) {
            holder.mainView.setBackgroundColor(getSelectionColor());
        } else {
            holder.mainView.setBackgroundColor(getNormalColor());
        }





    }


    @Override
    public int getItemCount() {
        return gestures.size();
    }


}
