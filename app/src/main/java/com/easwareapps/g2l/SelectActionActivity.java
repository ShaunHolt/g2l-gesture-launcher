/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l;


import java.util.List;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.easwareapps.g2l.fragment.SelectAppFragment;
import com.easwareapps.g2l.fragment.SelectContactFragment;
import com.easwareapps.g2l.fragment.SelectSpecificActionFragment;
import com.easwareapps.g2l.fragment.SelectURLFragment;
import com.easwareapps.g2l.utils.DataTransfer;


public class SelectActionActivity extends AppCompatActivity {

    boolean twoPane = false;
    int SELECT_SPECIFIC_ACTION=0;
    SelectAppFragment appFragment = null;
    SelectContactFragment contactFragment = null;
    SelectURLFragment urlFragment = null;

    private Toolbar toolbar =null;

    public static final int REQUEST_CONTACT = 225;
    public static final int REQUEST_CAMERA = 226;

    public void onCreate(Bundle savedInstanceState) {
        SharedPreferences pref = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        int theme = pref.getBoolean("enable_dark_theme", false) ? R.style.G2LDarkTheme_NoActionBar:
                R.style.G2LTheme_NoActionBar ;
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_action_list);
        if(findViewById(R.id.specific_action_list)!=null){
            twoPane = true;
        }

        
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null) {
            toolbar.setTitle(getResources().getString(R.string.select_action));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationIcon(R.mipmap.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }


        ListView actionListView = (ListView)findViewById(R.id.action_list);
        String[] actionList = getResources().getStringArray(R.array.action_list);
        actionListView.setAdapter(new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1,
                actionList));
		/*
		{
			@Override
            public boolean isEnabled(int position) {
                if(position == 7 && !hasFlash()){
                	return false;
                }
                return true;
            }
		}
		*/
        actionListView.setOnItemClickListener(new OnItemClickListener() {




            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                DataTransfer.itemSelected = position;
                if(position == 7 && !hasFlash()){
                    return;
                }
                if(twoPane){
                    String[] title = getResources().getStringArray(R.array.selection_list);
                    if(toolbar != null){
                        //toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
                        toolbar.setTitle(title[position]);
                    }
                    if(position == SelectSpecificActionFragment.APPS){
                        if(appFragment == null)
                            appFragment = new SelectAppFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_list, appFragment).commit();
                    }else if(position == SelectSpecificActionFragment.CALL_MSG_MAIL){
                        if(contactFragment == null)
                            contactFragment = new SelectContactFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_list, contactFragment).commit();
                    }else if(position == SelectSpecificActionFragment.URL){
                        if(urlFragment == null)
                            urlFragment = new SelectURLFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_list, urlFragment).commit();
                    }else{

                        SelectSpecificActionFragment fragment= new SelectSpecificActionFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_list, fragment).commit();
                    }
                }else{
                    Intent i = new Intent(getApplicationContext(), SpecificActionActivity.class);
                    i.putExtra("ACTION_ID", position);
                    startActivityForResult(i, SELECT_SPECIFIC_ACTION);
                }
                // TODO Auto-generated method stub
            }
        });

    }







    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if(intent != null && intent.getIntExtra("action", -4) != -4 ){
            try{
                Intent databackIntent = new Intent();
                databackIntent.putExtra("action", intent.getIntExtra("action", -4));
                databackIntent.putExtra("option", intent.getStringExtra("option"));
                databackIntent.putExtra("desc", intent.getStringExtra("desc"));
                setResult(0, databackIntent);
            }catch(Exception e){

            }
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent databackIntent = new Intent();
        databackIntent.putExtra("action", -4);
        Log.d("Action 1", "");
        databackIntent.putExtra("option", "");
        databackIntent.putExtra("desc", "");
        super.onBackPressed();
    }


    private boolean hasFlash() {


        if(!checkCameraPermissionGranted()) {

            return false;
        }

        PackageManager pm = this.getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return false;
        }
        Camera camera = Camera.open();
        Parameters p = camera.getParameters();
        List<String> flashModes = p.getSupportedFlashModes();
        if(flashModes==null){
            camera.release();
            return false;
        }else
        {
            camera.release();
            return true;
        }

    }

    @TargetApi(23)
    private boolean checkCameraPermissionGranted() {

        String permissions[] = new String[]{Manifest.permission.CAMERA};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        requestPermissions(permissions, REQUEST_CAMERA);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (isPermissionGranted(Manifest.permission.CAMERA)) {
            if(twoPane) {
                SelectSpecificActionFragment fragment = new SelectSpecificActionFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.specific_action_list, fragment).commit();
            } else {
                Intent i = new Intent(getApplicationContext(), SpecificActionActivity.class);
                i.putExtra("ACTION_ID", 7);
                startActivityForResult(i, SELECT_SPECIFIC_ACTION);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



}
