/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * G2L - A Gesture Launcher with customisation and privacy
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.g2l.fragment;

import com.easwareapps.g2l.R;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.ColorPicker.OnColorChangedListener;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

public class GestureSettingFragment extends Fragment 
	implements OnColorChangedListener, OnCheckedChangeListener,
		OnSeekBarChangeListener {

	Switch chkEnableMultipleStrokes = null;
	Switch chkClose = null;
	SeekBar seekTimeDely = null;
	SeekBar seekSensitivity =null; 
	ColorPicker picker = null;
	TextView txtTimeDely = null;
	TextView txtSensitivity = null;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.gesture_settings_fragment, container,
				false);
		
		
		chkEnableMultipleStrokes = (Switch) rootView.findViewById(R.id.chkEnableMultipleStrokes);
		chkClose = (Switch)rootView.findViewById(R.id.chkEnableClose);
		seekTimeDely = (SeekBar)rootView.findViewById(R.id.seekTimeDely);
		seekSensitivity = (SeekBar)rootView.findViewById(R.id.seekGestureSensitivity);
		txtTimeDely = (TextView)rootView.findViewById(R.id.txtTimeDely);
		txtSensitivity = (TextView)rootView.findViewById(R.id.txtSensitivity);
		
		chkEnableMultipleStrokes.setOnCheckedChangeListener(this);
		chkClose.setOnCheckedChangeListener(this);
		
		seekTimeDely.setOnSeekBarChangeListener(this);
		seekSensitivity.setOnSeekBarChangeListener(this);
		
		
		picker = (ColorPicker) rootView.findViewById(R.id.picker);
		picker.getColor();

		picker.setOldCenterColor(picker.getColor());
		picker.setOnColorChangedListener(this);

		picker.setShowOldCenterColor(false);
		
		initUI();
		

		return rootView;
	}

	private void initUI() {
		
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		int color = g2lPref.getInt("gesture_color", Color.YELLOW);
		picker.setColor(color);
		
		boolean state = g2lPref.getBoolean("enable_multiple_strokes", true);
		chkEnableMultipleStrokes.setChecked(state);
		
		state = g2lPref.getBoolean("finish_activity_after_launch", true);
		chkClose.setChecked(state);
		
		int value = g2lPref.getInt("stroke_fade_time", 2);
		seekTimeDely.setProgress(value);
		txtTimeDely.setText(String.format(getResources().getString(R.string.title_time_delay), "" +
				((value+1)*250)));
		
		value = g2lPref.getInt("gesture_sensitivity", 2);
		seekSensitivity.setProgress(value-1);
		txtSensitivity.setText(String.format(getResources().getString(R.string.title_gesture_sensitivity), value + ""));
		
		
		
	}

	@Override
	public void onColorChanged(int color) {
		Log.d("COLOR", color+"");
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		g2lEditor.putInt("gesture_color", color);
		g2lEditor.commit();
		
		
	}

	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked) {
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		if(view == chkEnableMultipleStrokes){
			seekTimeDely.setEnabled(isChecked);
			g2lEditor.putBoolean("enable_multiple_strokes", isChecked);
		}else if(view == chkClose){
			g2lEditor.putBoolean("enable_g2l_close", isChecked);
		}else{
			g2lEditor.commit();
			return;
		}
		g2lEditor.commit();
		
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int value,
			boolean fromUser) {
		String key = "stroke_fade_time";
		if(seekBar == seekTimeDely){
			key = "stroke_fade_time";
			txtTimeDely.setText(String.format(getResources().getString(R.string.title_time_delay), ((value+1)*250) + ""));
		}else if(seekBar == seekSensitivity){
			key = "gesture_sensitivity";
			txtSensitivity.setText(String.format(getResources().getString(R.string.title_gesture_sensitivity), (value+1) + ""));
		}else{
			return;
		}
		SharedPreferences g2lPref = getActivity().getSharedPreferences("com.easwareapps.g2l", Context.MODE_PRIVATE);
		SharedPreferences.Editor g2lEditor =g2lPref.edit();
		g2lEditor.putInt(key, value);
		g2lEditor.commit();
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

		
	}

}
